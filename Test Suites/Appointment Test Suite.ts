<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Appointment Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>c1f48a5c-73f6-4590-8a50-8f3801890c08</testSuiteGuid>
   <testCaseLink>
      <guid>30270b8e-d846-48f6-b74c-26790469273f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change password character</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ce63152d-53c9-4f35-b865-96c3a6a7a5a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change password length less than 8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85be7170-e48b-4948-9fbe-61655eb38639</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change password numbers</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96a121f2-4f9d-4be5-be6f-2aa665657779</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change password without special characters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3817e06f-571a-4b7f-a680-ce627152afd1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Fail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3cc795b2-3b5c-4d33-9683-75bb01128ab1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Success</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c56385e6-e806-4c5b-baf2-a845db242048</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>dab836fd-7910-438f-9277-d0fd84e7bb78</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4fd763ca-36ed-4cf0-87d6-01acc929b05b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6ba63ff0-30c8-4e1b-b83c-27fba2469446</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Password change</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
